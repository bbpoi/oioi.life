import { GetStaticProps } from 'next';
import Layout from '../components/Layout';
import { getAllPosts } from '../lib/api';
import { PostType } from '../types/post';
import Left from '../components/Left';
import Right from '../components/Right';
import Cards from '../components/Cards';
import Header from '../components/Header';
import Gallery from '../components/Gallery';
import Navigation from '../components/Navigation';

import { signIn, signOut, useSession } from 'next-auth/client';
import React, { useEffect } from 'react';
import Router from 'next/router';

type IndexProps = {
  posts: PostType[];
};

export const Index = ({ posts }: IndexProps): JSX.Element => {
  const [session, loading] = useSession();

  return (
    <Layout>
      {!session && (
        <>
          {/* Not signed in <br /> */}
          <div className="flex w-full h-screen">
            <button
              className="mx-auto self-center justify-self-center text-7xl font-medium tracking-wider text-dark hover:text-pink"
              onClick={() => signIn()}
            >
              Sign in
            </button>
          </div>
        </>
      )}
      {session && (
        <>
          {/* Signed in as {session.user.email} <br /> */}
          <button onClick={() => signOut()}>Sign out</button>
          <Navigation />
          <Left>
            <section className="h-screen grid grid-rows-5 grid-flow-col gap-4 w-full">
              <Header />
              <div className="w-full row-span-1 ">
                <Cards posts={posts} />
              </div>
              <div className="w-full row-span-3">bottom text</div>
            </section>
          </Left>
          <Right>
            <Gallery />
          </Right>
        </>
      )}
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const posts = getAllPosts(['date', 'description', 'slug', 'title']);

  return {
    props: { posts },
  };
};

export default Index;

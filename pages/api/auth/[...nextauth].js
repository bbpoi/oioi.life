import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'

export default NextAuth({
  // Configure one or more authentication providers
  providers: [
    Providers.Credentials({
      // The name to display on the sign in form (e.g. 'Sign in with...')
      name: 'luck',
      // The credentials is used to generate a suitable form on the sign in page.
      // You can specify whatever fields you are expecting to be submitted.
      // e.g. domain, username, password, 2FA token, etc.
      credentials: {
        // username: { label: "Username", type: "text", placeholder: "username" },
        password: {  label: "Password", type: "password" }
      },
     async authorize(credentials) {
        // const user = (credentials) => {
        //   // You need to provide your own logic here that takes the credentials
        //   // submitted and returns either a object representing a user or value
        //   // that is false/null if the credentials are invalid.
        //   // e.g. return { id: 1, name: 'J Smith', email: 'jsmith@example.com' }
        //   if (credentials.username === 'leriaz' ){
        //     if (credentials.password === 'paokara4'){
        //       return {id: 1, name:'leriaz'}
        //     }
        //   } 
        //   return null
        // }
        // if (user != null) {
        //   // Any user object returned here will be saved in the JSON Web Token
        //   console.log(user)
        //   return user
        // } else {
        //   return null
        // }
        if ((credentials.password === process.env.PASSWD)){
          return {id:1, name:'leriaz'}
        } else{
          return null
        }
      }
    })
  ],
  pages: {
    signIn:'/auth/signin'
  }

  // A database is optional, but required to persist accounts in a database
  // database: process.env.DATABASE_URL,
})
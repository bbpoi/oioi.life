import type { AppProps } from 'next/app';
import React from 'react';
import 'tailwindcss/tailwind.css';
import '../styles/globals.css';
import 'tailwind-scrollbar';
import { Provider } from 'next-auth/client';

const MyApp = ({ Component, pageProps }: AppProps): JSX.Element => {
  return (
    <Provider session={pageProps.session}>
      <Component {...pageProps} />
    </Provider>
  );
};

export default MyApp;

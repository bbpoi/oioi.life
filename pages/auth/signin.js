import { getCsrfToken } from 'next-auth/client';

export default function SignIn({ csrfToken }) {
  return (
    <div className="flex w-full h-screen">

    <form method="post" action="/api/auth/callback/credentials" className="mx-auto self-center justify-self-center text-5xl">
      <input name="csrfToken" type="hidden" defaultValue={csrfToken} />
     
        <input name="password" type="password"  placeholder="password" className="border-2 border-dark text-center"/>
      <br/>
      <button type="submit" className="bg-pink text-white w-full mt-2 p-4 hover:bg-link">Sign in</button>
    </form>
    </div>
  );
}

// This is the recommended way for Next.js 9.3 or newer
export async function getServerSideProps(context) {
  return {
    props: {
      csrfToken: await getCsrfToken(context),
    },
  };
}

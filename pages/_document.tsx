import React from 'react';
import Document, { Head, Html, Main, NextScript } from 'next/document';
class MyDocument extends Document {
  render(): JSX.Element {
    return (
      <Html lang="en">
        <Head />
        {/* <Navigation /> */}
        <body className="font-jetbrains bg-custom">
          {/* patterns: ["jupiter", "hideout", "bank-note","random-shapes","squares"], */}
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;

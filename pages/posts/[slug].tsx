import fs from 'fs';
import matter from 'gray-matter';
import mdxPrism from 'mdx-prism';
import { GetStaticPaths, GetStaticProps } from 'next';
import hydrate from 'next-mdx-remote/hydrate';
import renderToString from 'next-mdx-remote/render-to-string';
import { MdxRemote } from 'next-mdx-remote/types';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import path from 'path';
import React from 'react';
import rehypeAutolinkHeadings from 'rehype-autolink-headings';
import rehypeSlug from 'rehype-slug';
import Layout, { MetaProps, WEBSITE_HOST_URL } from '../../components/Layout';
import { PostType } from '../../types/post';
import { postFilePaths, POSTS_PATH } from '../../utils/mdxUtils';
import Left from '../../components/Left';
import Right from '../../components/Right';
import { getAllPosts } from '../../lib/api';
import Cards from '../../components/Cards';
import Header from '../../components/Header';
import Gallery from '../../components/Gallery';
import Navigation from '../../components/Navigation';
import { signIn, signOut, useSession } from 'next-auth/client';

const components = {
  Head,
  Image,
  Link,
};

type PostPageProps = {
  source: MdxRemote.Source;
  frontMatter: PostType;
  posts: PostType[];
};

const PostPage = ({
  source,
  frontMatter,
  posts,
}: PostPageProps): JSX.Element => {
  const content = hydrate(source, { components });
  const customMeta: MetaProps = {
    title: `${frontMatter.title}`,
    description: frontMatter.description,
    image: `${WEBSITE_HOST_URL}${frontMatter.image}`,
    date: frontMatter.date,
    type: 'article',
  };
  const [session, loading] = useSession();

  return (
    <Layout customMeta={customMeta}>
      {!session && (
        <>
         <div className="flex w-full h-screen">
            <button
              className="mx-auto self-center justify-self-center text-7xl font-medium tracking-wider text-dark hover:text-pink"
              onClick={() => signIn()}
            >
              Sign in
            </button>
          </div>
        </>
      )}
      {session && (
        <>
          {/* Signed in as {session.user.email} <br /> */}
          <button onClick={() => signOut()}>Sign out</button>
          <Navigation />
          <Left>
            <section className="h-screen grid grid-rows-5 grid-flow-col gap-4 w-full ">
              <Header />
              <div className="w-full row-span-1 ">
                <Cards posts={posts} />
              </div>
              <div className="w-full row-span-3 p-4 overflow-scroll scrollbar-thin scrollbar-thumb-bright scrollbar-track-dark">
                <article>
                  <h1 className="mb-3 text-gray-900 dark:text-white text-center text-3xl">
                    {frontMatter.title}
                  </h1>
                  {/* <p className="mb-10 text-sm text-gray-500 dark:text-gray-400">
                {format(parseISO(frontMatter.date), 'MMMM dd, yyyy')}
              </p> */}
                  <div className="mx-auto p-8 prose md:max-w-screen-xl max-w-full-sm ">
                    {content}
                  </div>
                </article>
              </div>
            </section>
          </Left>
          <Right>
            <Gallery />
          </Right>
        </>
      )}
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const postFilePath = path.join(POSTS_PATH, `${params.slug}.mdx`);
  const source = fs.readFileSync(postFilePath);
  const posts = getAllPosts(['date', 'description', 'slug', 'title']);

  const { content, data } = matter(source);

  const mdxSource = await renderToString(content, {
    components,
    // Optionally pass remark/rehype plugins
    mdxOptions: {
      remarkPlugins: [require('remark-code-titles')],
      rehypePlugins: [mdxPrism, rehypeSlug, rehypeAutolinkHeadings],
    },
    scope: data,
  });

  return {
    props: {
      source: mdxSource,
      frontMatter: data,
      posts,
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = postFilePaths
    // Remove file extensions for page paths
    .map((path) => path.replace(/\.mdx?$/, ''))
    // Map the path into the static paths object required by Next.js
    .map((slug) => ({ params: { slug } }));

  return {
    paths,
    fallback: false,
  };
};

export default PostPage;

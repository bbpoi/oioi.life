const { spacing } = require('tailwindcss/defaultTheme');

module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    colors: {
      black: '#1f1f1f',
      dark: '#333333',
      fade: '#ffecd644',
      white: '#e3e3e3',
      txt: '#ffd4a3',
      post: '#e8d2bb',
      red: '#ff001e',
      pink: '#ff2b44',
      muddy: '#80a787',
      bright: '#1cffe5',
      link: '#2ce497',
    },
    extend: {
      fontFamily: {
        jetbrains: ['JetBrains Mono', 'monospace'],
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            color: theme('colors.dark'),
            a: {
              color: theme('colors.pink'),
              'text-decoration': 'none',
              '&:hover': {
                color: theme('colors.link'),
              },
              code: { color: theme('colors.fade') },
            },
            'h1,h2,h3,h4': {
              'scroll-margin-top': spacing[32],
              'text-align': 'center',
              color: theme('colors.black'),
            },
            code: { color: theme('colors.pink.500') },
            'blockquote p:first-of-type::before': false,
            'blockquote p:last-of-type::after': false,
          },
        },
      }),
    },
  },
  variants: {
    typography: ['dark'],
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('tailwind-scrollbar'),
    require('tailwind-heropatterns')({
      variants: [],
      patterns: [
        'jupiter',
        'hideout',
        'bank-note',
        'random-shapes',
        'squares',
        'graph-paper',
      ],

      colors: {
        default: '#212121',
      },

      opacity: {
        default: '0.3',
      },
    }),
  ],
};

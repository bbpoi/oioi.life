import Head from 'next/head';
import { useRouter } from 'next/router';
import React from 'react';
import NextNprogress from 'nextjs-progressbar';
import { PostType } from '../types/post';
export interface MetaProps
  extends Pick<PostType, 'date' | 'description' | 'image' | 'title'> {
  /**
   * For the meta tag `og:type`
   */
  type?: string;
}

type LayoutProps = {
  children: React.ReactNode;
  customMeta?: MetaProps;
};

export const WEBSITE_HOST_URL = '';

const Layout = ({ children, customMeta }: LayoutProps): JSX.Element => {
  const router = useRouter();
  const meta: MetaProps = {
    title: 'oioi',
    description: 'oioi',
    // image: `${WEBSITE_HOST_URL}/images/site-preview.png`,
    type: 'website',
    ...customMeta,
  };

  return (
    <>
      <Head>
        <title>{meta.title}</title>
        <meta content={meta.description} name="description" />
        <meta
          property="og:url"
          content={`${WEBSITE_HOST_URL}${router.asPath}`}
        />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=JetBrains+Mono:wght@200;300;400;800&display=swap"
          rel="stylesheet"
        />
        <link rel="canonical" href={`${WEBSITE_HOST_URL}${router.asPath}`} />
        <meta property="og:type" content={meta.type} />
        <meta property="og:site_name" content="oioi" />
        <meta property="og:description" content={meta.description} />
        <meta property="og:title" content={meta.title} />
        <meta property="og:image" content={meta.image} />
        {meta.date && (
          <meta property="article:published_time" content={meta.date} />
        )}
      </Head>
      <NextNprogress
        color="#1cffe5"
        startPosition={0.3}
        stopDelayMs={200}
        height={3}
      />

      <main className="container mx-auto p-2 max-w-full max-h-screen overflow-auto md:overflow-hidden">
        <div className="flex flex-wrap">{children}</div>
      </main>
    </>
  );
};

export default Layout;

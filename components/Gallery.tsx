import React from 'react';

const Gallery = (): JSX.Element => {
  return (
    <div className="flex flex-col">
      {/* -------------------------------------------------------------------- */}
      <section className="grid grid-cols-2 grid-flow-col gap-2 w-full pt-10 my-2">
        <div className="">
          <h2 className="text-center text-4xl font-medium">Some title </h2>
          <p>Paragraph. Text. Lots of text. Or not.</p>
        </div>
        <div className="border-4">
          <div
            id="media-title"
            className="w-full h-8 text-xl text-center border-b-4"
          >
            Media Title
          </div>
          <div
            id="media"
            className="bg-hero-graph-paper bg-white text-black h-52"
          >
            <img
              src="https://ipsm.io/api/image/200x200/c-718096/jpg"
              className="object-cover h-full w-full"
            />
          </div>
        </div>
      </section>
      {/* -------------------------------------------------------------------- */}
      <section className="grid grid-cols-2 grid-flow-col gap-2 w-full my-2">
        <div className="">
          <h2 className="text-center text-4xl font-medium">βιντεό</h2>
        </div>
        <div className="border-4">
          <div id="media-title" className="w-full h-8 text-center border-b-4">
            title
          </div>
          <div
            id="media"
            className="bg-hero-graph-paper bg-white text-black h-52"
          >
            <video controls className="object-cover h-full w-full">
              <source
                src="https://ipsm.io/api/video/360p/mp4"
                type="video/mp4"
              />
            </video>
          </div>
        </div>
      </section>
      {/* -------------------------------------------------------------------- */}
      <section className="grid grid-cols-2 grid-flow-col gap-2 w-full my-2">
        <div className="">
          <h2 className="text-center text-4xl font-medium">
            letters<sup>TM</sup>
          </h2>
        </div>
        <div className="border-4">
          <div id="media-title" className="w-full h-8 text-center border-b-4">
            title
          </div>
          <div
            id="media"
            className="bg-hero-graph-paper bg-white text-black h-52"
          ></div>
        </div>
      </section>
      {/* -------------------------------------------------------------------- */}
    </div>
  );
};

export default Gallery;

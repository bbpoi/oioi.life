import React from 'react';

type LayoutProps = {
  children: React.ReactNode;
};
const Left = ({ children }: LayoutProps): JSX.Element => {
  return (
    //md:w-1/2
    // <header className="w-full md:w-1/2 pr-0 md:pr-4 order-1 md:order-1 overflow-scroll scrollbar-thin scrollbar-thumb-pink scrollbar-track-fade">
    <header
      id="left"
      className="w-full md:w-1/2 pr-0 md:pr-4 order-1 md:order-1 overflow-hidden"
    >
      <div className="w-full leading-loose tracking-tight p-4">{children}</div>
    </header>
  );
};

export default Left;

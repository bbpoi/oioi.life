import Link from 'next/link';
import React from 'react';
import { PostType } from '../types/post';
import { useRouter } from 'next/router';

type IndexProps = {
  posts: PostType[];
};

export const Cards = ({ posts }: IndexProps): JSX.Element => {
  const router = useRouter();
  return (
    <div className="flex flex-wrap -m-2 p-2">
      {/* ---------- */}

      {posts.map((post) => (
        <div key={post.slug} className="p-2 lg:w-1/4 md:w-1/2 w-1/2">
          <div className="h-full flex items-center p-4">
            <Link as={`/posts/${post.slug}`} href={`/posts/[slug]`}>
              <a
                className={
                  'square mx-auto ' +
                  (router.query.slug === post.slug ? 'active-square' : '')
                }
              ></a>
            </Link>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Cards;

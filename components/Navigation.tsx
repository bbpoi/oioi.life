import React from 'react';

function useStickyState(defaultValue, key) {
  const [value, setValue] = React.useState(defaultValue);

  React.useEffect(() => {
    const stickyValue = window.localStorage.getItem(key);

    if (stickyValue !== null) {
      setValue(JSON.parse(stickyValue));
    }
  }, [key]);

  React.useEffect(() => {
    window.localStorage.setItem(key, JSON.stringify(value));
  }, [key, value]);

  return [value, setValue];
}

const Navigation = (): JSX.Element => {
  const [left, setLeft] = useStickyState(false, 'left');
  const [right, setRight] = useStickyState(false, 'right');

  const [leftButton, setLeftButton] = React.useState('<-');
  const [rightButton, setRightButton] = React.useState('->');

  // ! SO MESSY
  // TODO: Change to a direction based system (?) -1 => +1
  React.useEffect(() => {
    if (left) {
      setLeftButton('->');
      document.getElementById('righty').classList.add('hidden');
      document.getElementById('left').classList.add('hidden');
      document.getElementById('right').classList.remove('md:w-1/2');
    } else {
      setLeftButton('<-');

      document.getElementById('righty').classList.remove('hidden');
      document.getElementById('left').classList.remove('hidden');
      document.getElementById('right').classList.add('md:w-1/2');
    }
    if (right) {
      setRightButton('<-');
      document.getElementById('lefty').classList.add('hidden');
      document.getElementById('right').classList.add('hidden');
      document.getElementById('left').classList.remove('md:w-1/2');
    } else {
      setRightButton('->');
      document.getElementById('lefty').classList.remove('hidden');
      document.getElementById('right').classList.remove('hidden');
      document.getElementById('left').classList.add('md:w-1/2');
    }
  });

  function moveLeft() {
    setLeft(!left);
  }
  function moveRight() {
    setRight(!right);
  }

  return (
    <nav className="w-full text-center top-0 h-4">
      <button
        id="lefty"
        onClick={moveLeft}
        className="text-dark font-bold hover:text-bright"
      >
        {leftButton}
      </button>
      &nbsp;
      <button
        id="righty"
        onClick={moveRight}
        className="text-dark font-bold hover:text-bright"
      >
        {rightButton}
      </button>
    </nav>
  );
};
export default Navigation;

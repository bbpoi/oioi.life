import Link from 'next/link';
import React from 'react';

const Header = (): JSX.Element => {
  return (
    <header className="w-full row-span-1 text-center text-5xl pt-10">
      <Link href="/">
        <a className="text-black hover:text-bright">[[[ oioi ]]]</a>
      </Link>
    </header>
  );
};

export default Header;

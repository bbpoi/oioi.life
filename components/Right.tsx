import React from 'react';

type LayoutProps = {
  children: React.ReactNode;
};
const Right = ({ children }: LayoutProps): JSX.Element => {
  return (
    //md:w-1/2
    <section
      id="right"
      className="w-full md:w-1/2  md:bottom-12 order-1 md:order-2 h-screen overflow-scroll scrollbar-thin scrollbar-thumb-bright scrollbar-track-fade"
    >
      <div className="leading-loose tracking-tight p-4">{children}</div>
    </section>
  );
};

export default Right;
